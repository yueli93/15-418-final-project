#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include <array>
#include <limits>
#include <map>
#include <math.h>
#include <vec3.h>
#include <CycleTimer.h>
#include <MarchingCubes.h>

#define X 1
#define Y 2
#define Z 3
#define scaleFactor 2
#define INOUT 1
#define ISDONE 2
#define ISBOUND 4

using namespace std;

struct atom {
    string elem;
    int serial;
    double x;
    double y;
    double z;
};

enum SurfaceType {VDW=1, MS=2, SAS=3, SES=4};

typedef std::numeric_limits< double > dbl;
using atoms = vector<atom>;
using extents = array<array<double,3>,3>;
using neighbors = array<array<int,3>,26>;
using atomTypes = array<string,17>;


/*
 * loadAtomsFromFile: reads a given ifstream of a pdb file
 * and parses the atom data
 * TESTED
 */
atoms loadAtomsFromFile(ifstream &pdb_file) {
    vector<atom> result;
    string line;
    int serial = 0;
    while (getline(pdb_file,line)) {
      vector<string> row;
      string phrase;
      stringstream ss(line);
      while(getline(ss,phrase,'\t')) {
        row.push_back(move(phrase));
      }
      atom newAtom;
      newAtom.x = stod(row[0]);
      newAtom.y = stod(row[1]);
      newAtom.z = stod(row[2]);
      newAtom.elem = row[3];
      newAtom.serial = serial;
      result.push_back(move(newAtom));
      serial++;
    }
    return result;
}

// returns [[xmin,ymin,zmin],[xmax,ymax,zmax][xavg,yavg,zavg]]
/*
* TESTED
*/
extents getExtent(atoms atomList) {
    double xmin, ymin, zmin;
    double xmax, ymax, zmax;
    double xsum, ysum, zsum;
    double cnt;
    double atomX, atomY, atomZ;

    xmin = ymin = zmin = numeric_limits<double>::max();
    xmax = ymax = zmax = numeric_limits<double>::min();
    xsum = ysum = zsum = cnt = double(0.0);

    if (atomList.size() == 0) {
        return {{{0,0,0},{0,0,0},{0,0,0}}};
    }
    for (int i = 0; i < (int) atomList.size(); i++) {
        atom a = atomList[i];

        cnt++;

        //we might get an error here
        atomX = a.x;
        atomY = a.y;
        atomZ = a.z;

        xsum += atomX;
        ysum += atomY;
        zsum += atomZ;

        xmin = (xmin < atomX) ? xmin : atomX;
        ymin = (ymin < atomY) ? ymin : atomY;
        zmin = (zmin < atomZ) ? zmin : atomZ;

        xmax = (xmax > atomX) ? xmax : atomX;
        ymax = (ymax > atomY) ? ymax : atomY;
        zmax = (zmax > atomZ) ? zmax : atomZ;

    }
    //can we verify that xsum/cnt is the same in javascript and c++?
    extents result;
    result[0] = {xmin, ymin, zmin};
    result[1] = {xmax, ymax, zmax};
    result[2] = {xsum/cnt, ysum/cnt, zsum/cnt};
    return result;
    // return {{{xmin,ymin,zmin},{xmax,ymax,zmax},{xsum/cnt,ysum/cnt,zsum/cnt}}};
}



class ProteinSurface {
    atoms A;
    double margin;
    double pminx, pminy, pminz;
    double pmaxx, pmaxy, pmaxz;
    double ptranx, ptrany, ptranz;
    int pLength, pWidth, pHeight;
    //might not need this one
    double cutRadius;

    vector<vector<vector<uint8_t>>> vpBits;
    vector<vector<vector<double>>> vpDistance;
    vector<vector<vector<int>>> vpAtomID;
    //might need to change the types of these above & below
    double vertnumber, facenumber;
    vector<vec3> verts;
    vector<int> faces;

    extents origextent;
    // what do these do?
    // map<string,vector<vector<int>>> depty;
    map<string,vector<int>> depty;

    map<string,int> widxz;
    // var faces, verts;

    MarchingCubes mc;
    atomTypes atomType = {{"H","Li","Na","K","C","N","O","F","P","S","CL","BR","SE",
                            "ZN","CU","NI","X"}};

    map<string,double> atomRadii = {
        {"H", 1.2},
        {"Li", 1.82},
        {"Na", 2.27},
        {"K", 2.75},
        {"C", 1.7},
        {"N", 1.55},
        {"O", 1.52},
        {"F", 1.47},
        {"P", 1.80},
        {"S", 1.80},
        {"CL", 1.75},
        {"BR", 1.85},
        {"SE", 1.90},
        {"ZN", 1.39},
        {"CU", 1.4},
        {"NI", 1.63},
        {"X", 2},
    };

    // neighbors nb = {{
    //     //bottom layer
    //     {{-1,-1,-1}},{{0,-1,-1}},{{1,-1,-1}},{{-1,0,-1}},{{0,0,-1}},{{1,0,-1}},{{-1,1,-1}},{{0,1,-1}},{{1,1,-1}},
    //     //middle layer
    //     {{-1,-1,0}},{{0,-1,0}},{{1,-1,0}},{{-1,0,0}},{{1,0,0}},{{-1,1,0}},{{0,1,0}},{{1,1,0}},
    //     //top layer
    //     {{-1,-1,1}},{{0,-1,1}},{{1,-1,1}},{{-1,0,1}},{{0,0,1}},{{1,0,1}},{{-1,1,1}},{{0,1,1}},{{1,1,1}}
    // }};
    neighbors nb = {{
     {{ 1, 0, 0 }},
     {{ -1, 0, 0 }},
     {{ 0, 1, 0 }},
     {{ 0, -1, 0 }},
     {{ 0, 0, 1 }},
     {{ 0, 0, -1 }},
     {{ 1, 1, 0 }},
     {{ 1, -1, 0 }},
     {{ -1, 1, 0 }},
     {{ -1, -1, 0 }},
     {{ 1, 0, 1 }},
     {{ 1, 0, -1 }},
     {{ -1, 0, 1 }},
     {{ -1, 0, -1 }},
     {{ 0, 1, 1 }},
     {{ 0, 1, -1 }},
     {{ 0, -1, 1 }},
     {{ 0, -1, -1 }},
     {{ 1, 1, 1 }},
     {{ 1, 1, -1 }},
     {{ 1, -1, 1 }},
     {{ -1, 1, 1 }},
     {{ 1, -1, -1 }},
     {{ -1, -1, 1 }},
     {{ -1, 1, -1 }},
     {{ -1, -1, -1 }}
}};
    //return radius of atom according to the atom name.
    //warning might have to do null checking
    string getVDWIndex(atom a) {
        if (atomRadii[a.elem] == 0)
            return "X";
        else
            return a.elem;
    }


    //should make this public...
    public:
        void initParm(extents e, atoms a) {
            A = a;
            //margin to avoid boundry round off effects
            //we should do type casting?
            margin = (1.0/scaleFactor) * 5.5;
            //this should be a field in the class
            origextent = e;
            //these are also fields in the class
            pminx = e[0][0] - margin;
            pminy = e[0][1] - margin;
            pminz = e[0][2] - margin;

            pmaxx = e[1][0] + margin;
            pmaxy = e[1][1] + margin;
            pmaxz = e[1][2] + margin;

            pminx = floor(pminx * scaleFactor) / scaleFactor;
            pminy = floor(pminy * scaleFactor) / scaleFactor;
            pminz = floor(pminz * scaleFactor) / scaleFactor;
            pmaxx = ceil(pmaxx * scaleFactor) / scaleFactor;
            pmaxy = ceil(pmaxy * scaleFactor) / scaleFactor;
            pmaxz = ceil(pmaxz * scaleFactor) / scaleFactor;


            ptranx = -pminx;
            ptrany = -pminy;
            ptranz = -pminz;

            pLength = (int) ceil(scaleFactor * (pmaxx - pminx)) + 1;
            pWidth = (int) ceil(scaleFactor * (pmaxy - pminy)) + 1;
            pHeight = (int) ceil(scaleFactor * (pmaxz - pminz)) + 1;



            boundingAtom();


            vpBits = vector<vector<vector<uint8_t>>> (pLength, vector<vector<uint8_t>>(pWidth, vector<uint8_t>(pHeight)));
            vpDistance = vector<vector<vector<double>>> (pLength, vector<vector<double>>(pWidth, vector<double>(pHeight)));
            //vpAtomID keeps track of which atom is responsible for each voxel.
            vpAtomID = vector<vector<vector<int>>> (pLength, vector<vector<int>>(pWidth, vector<int>(pHeight)));

            //should print out length, width height here for sanity check.
        }

    /*
     * boundingAtom: sets up depty and widxz, which are used to fill the voxels within an atom sphere.
     * - for a given pos. in xz-plane, depty[pos] gives the height of the sphere above this pos, aka the distance
     * from the pos to the edge of the sphere
     * - widxz maps an atom to it's rounded square radius
     * TESTED
     */
    void boundingAtom() {
        map<string,double> tradius;
        int txz;
        double radius, scaledRadius, sradius, tdept;

        for (map<string,double>::const_iterator iter = atomRadii.begin(); iter != atomRadii.end(); ++iter) {
            string key = iter->first;
            // cout << key << endl ;

            radius = iter->second;
            scaledRadius = radius * scaleFactor + 0.5;
            // cout << scaledRadius << endl;

            tradius[key] = scaledRadius;
            sradius = scaledRadius * scaledRadius;
            widxz[key] = (int) floor(scaledRadius) + 1;
            // cout << key << '\t' << widxz[key] << endl;
            // cout << key << '\t';
            // depty[key] = vector<vector<int>> (widxz[key],vector<int>(widxz[key]));
            depty[key] = vector<int> (widxz[key]*widxz[key]);
            int indx = 0;
            for (int j = 0; j < widxz[key]; j++) {
                for (int k = 0; k < widxz[key]; k++) {
                    txz = j * j + k * k;
                    // indx is outside atom sphere
                    if ((double)txz > sradius) {
                        depty[key][indx] = -1;
                        // cout << -1 << endl;
                    }
                    // indx inside atom sphere,
                    // calculate dist. from indx to sphere edge
                    else {
                        tdept = sqrt(sradius - txz);
                        depty[key][indx] = floor(tdept);
                        // cout << floor(tdept) << endl;
                    }
                    // cout << depty[key][indx] << " ";
                    indx++;
                }
            }
            // cout << endl;
            // cout << depty[key] << endl;
            // cout << endl;

        }
    }

    void fillVoxels(atoms a){
        for (int i = 0; i < pLength; i++) {
            for (int j = 0; j < pWidth; j++) {
                for (int k = 0; k < pHeight; k ++) {
                    //need typecast?
                    vpBits[i][j][k] = 0;
                    vpDistance[i][j][k] = -1.0;
                    vpAtomID[i][j][k] = -1;
                }
            }
        }

        int numInout = 0;
        for (int i = 0; i < (int) a.size(); i++) {
            //null case check, which I don't think is possible
            numInout += fillAtom(a[i]);
        }
        // cout << "num inout: " << numInout << endl;
        int count = 0;

        for (int i = 0; i < pLength; i++) {
            for (int j = 0; j < pWidth; j++) {
                for (int k = 0; k < pHeight; k++) {
                    //need typecast?
                    if (vpBits[i][j][k] & INOUT) {
                        count += 1;
                        vpBits[i][j][k] |= ISDONE;
                    }
                }
            }
        }
    }

    // PARTIALLY TESTED
    int fillAtom(atom a) {
        //translate coordinates to our scale
        double cx = floor(0.5 + scaleFactor * (a.x + ptranx));
        double cy = floor(0.5 + scaleFactor * (a.y + ptrany));
        double cz = floor(0.5 + scaleFactor * (a.z + ptranz));

        // cout << a.elem << '\t' << cx << '\t' << cy << '\t' << cz << endl;

        string at = getVDWIndex(a);
        // string at = a.elem;
        // cout << a.elem << '\t' << atomRadii[at] << endl;
        int nind = 0;
        int numInout = 0;
        // int cnt = 0;
        // int pWH = pWidth * pHeight;
        // cout << at << '\t';
        // cout << at << " ";
        for (int i = 0; i < widxz[at]; i++) {
            for (int j = 0; j < widxz[at]; j++) {
                //if inside atom
                if (depty[at][nind] != -1) {
                    // cout << depty[at][nind] << " ";
                    for (int ii = -1; ii < 2; ii++) {
                        for (int jj = -1; jj < 2; jj++) {
                            for (int kk = -1; kk < 2; kk++) {
                                //not itself
                                if (ii != 0 && jj != 0 && kk!= 0) {
                                    int mi = ii * i;
                                    int mk = kk * j;
                                    for (int k = 0 ; k <= depty[at][nind]; k++) {

                                        int mj = k * jj;
                                        int si = cx + mi;
                                        int sj = cy + mj;
                                        int sk = cz + mk;
                                        if (si < 0 || sj < 0 || sk < 0 || si >= pLength || sj >= pWidth || sk >= pHeight) {
                                            continue;
                                        }
                                        // Mark (si, sj, sk) as inside atom sphere, and belonging to atom
                                        if (!(vpBits[si][sj][sk] & INOUT)) {
                                            vpBits[si][sj][sk] |= INOUT;
                                            vpAtomID[si][sj][sk] = a.serial;
                                            numInout++;
                                        }
                                        else {
                                            // find which atom is closer to this point
                                            atom a2 = A[vpAtomID[si][sj][sk]];
                                            int ox = (int) floor(0.5 + scaleFactor * (a2.x + ptranx));
                                            int oy = (int) floor(0.5 + scaleFactor * (a2.y + ptrany));
                                            int oz = (int) floor(0.5 + scaleFactor * (a2.z + ptranz));

                                            if (mi * mi + mj * mj + mk * mk < ox * ox + oy * oy + oz * oz) {
                                                vpAtomID[si][sj][sk] = a.serial;
                                                continue;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                nind++;
            }
        }
        return numInout;
    }

    //iterates around neighbors of a INOUT voxel and labels non-INOUT neighbors as ISBOUND
    /*
    * TESTED
    */
    void buildBoundry() {
        // int pWH = pWidth * pHeight;
        int count = 0;
        for (int i = 0; i < pLength; i++) {
            for (int j = 0; j < pWidth; j++) {
                for (int k = 0; k < pHeight; k++) {
                    if (vpBits[i][j][k] & INOUT){
                        int ii = 0;
                        while (ii < 26) {
                            int ti = i + nb[ii][0];
                            int tj = j + nb[ii][1];
                            int tk = k + nb[ii][2];
                            if ((ti > -1) && (ti < pLength) &&
                                (tj > -1) && (tj < pWidth) &&
                                (tk > -1) && (tk < pHeight)) {

                                if (!(vpBits[ti][tj][tk] & INOUT))
                                {
                                    if (!(vpBits[i][j][k] & ISBOUND))
                                        count++;
                                    vpBits[i][j][k] |= ISBOUND;

                                    break;
                                }
                                else {
                                    ii++;
                                }
                            }

                            else {
                                ii++;
                            }
                        }
                    }
                }
            }
        }
        cout << "isbound count" << '\t' << count << endl;
    }

    // void fastDistanceMap() {
    //     int pWH = pWidth * pHeight;
    //     boundPoint = vector<vector<vector<int>>> (pLength, vector<vector<int>>(pWidth, vector<int>(pHeight)));

    //     for (int i = 0; i < pLength; i++) {
    //         for (int j = 0; j < pWidth; j++) {
    //             for (int k = 0; k < pHeight; k++)
    //                 vpBits[i][j][k] &= ~ISDONE;
    //                 if (vpBits[i][j][k] & INOUT) {
    //                     if (vpBits[i][j][k] & ISBOUND) {

    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }

    void marchingCubeInit(SurfaceType stype) {
      int count = 0;
      int ifCount = 0;
      for(int i=0; i < pLength; i++) {
        for(int j=0; j < pWidth; j++) {
          for(int k=0; k < pHeight; k++) {
            uint8_t bitmask = vpBits[i][j][k];
            switch(stype) {
              case VDW:
                bitmask &= ~ISBOUND;
                break;
              case SES:
                bitmask &= ~ISDONE;
                if(bitmask & ISBOUND) {
                  bitmask |= ISDONE;
                  ifCount++;
                }
                count++;
                bitmask &= ~ISBOUND;
                break;
              case MS: // after VDW
                if((bitmask & ISBOUND) && (bitmask & ISDONE))
                  bitmask &= ~ISBOUND;
                else if( (bitmask & ISBOUND) && !(bitmask & ISDONE))
                  bitmask |= ISDONE;
                break;
              case SAS:
                bitmask &= ~ISBOUND;
            }
            vpBits[i][j][k] = bitmask;
          }
        }
      }

    }

    void marchingCube(SurfaceType stype) {
        marchingCubeInit(stype);
        marchOpts opts;
        opts.smooth = 1;
        opts.nX = pLength;
        opts.nY = pWidth;
        opts.nZ = pHeight;
        /*
        uint8_t *vpBits_1d = new uint8_t[pLength * pWidth * pHeight];
        int index = 0;
        for(int i=0; i<pLength; i++) {
            for(int j=0; j<pWidth; j++) {
                for(int k=0; k<pHeight; k++) {
                    vpBits_1d[index] = vpBits[i][j][k];
                    index++;
                }
            }
        }
        mc.march(vpBits_1d, opts);
        delete [] vpBits_1d;
        */
        mc.march(vpBits, opts);
        // TODO: print json of verts, faces
    }

    void laplacianSmooth(int numiter) {
        mc.laplacianSmooth(numiter);
    }
};

int main(int argc, char *argv[]) {
    string pdb_filename = "pdb_parsed/small";
    // check for pdb file in aommand line args
    if(argc > 1) {
      pdb_filename = argv[1];
    }
    ifstream pdb_file(pdb_filename);
    if(!pdb_file) {
      cerr << "Invalid PDB filename" << endl;
      exit(1);
    }
    // setup cout for printing double-wide numbers to max precision
    cout.precision(dbl::digits10);
    double startTime = CycleTimer::currentSeconds();
    atoms a = loadAtomsFromFile(pdb_file);
    double startExtent = CycleTimer::currentSeconds();
    extents e = getExtent(a);
    double endExtent = CycleTimer::currentSeconds();
    // cout << "extent" << endl;
    // for(int i=0; i<3; i++) {
    //   cout << "[";
    //   for(double value : e[i]) cout << value << " ";
    //   cout << "]" << endl;
    // }
    // double w = e[1][0] - e[0][0];
    // double h = e[1][1] - e[0][1];
    // double d = e[1][2] - e[0][2];
    // double vol = w*h*d;

    ProteinSurface ps;
    double startInitParam = CycleTimer::currentSeconds();
    ps.initParm(e, a);
    double endInitParam = CycleTimer::currentSeconds();
    ps.fillVoxels(a);
    // ps.buildBoundry();
    ps.marchingCube(VDW);
    double startLaplacian = CycleTimer::currentSeconds();
    ps.laplacianSmooth(1);
    double endTime = CycleTimer::currentSeconds();

    cout << "total sequential time: " << (startExtent - startTime) + (endInitParam - startInitParam) + (endTime - startLaplacian) << endl;
    cout << "total parallelizable time: " << (endExtent - startExtent) + (startLaplacian - endInitParam) << endl;
    // cout << w << "\t" << h << "\t" << d << endl;



    double duration = CycleTimer::currentSeconds() - startTime;
    cout << "duration: " << duration << endl;
    // delete e;
}


