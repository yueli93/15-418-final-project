#include <vector>
#include <vec3.h>
#ifndef MARCHING_CUBES_H
#define MARCHING_CUBES_H

#define INOUT 1
#define ISDONE 2
#define ISBOUND 4

using namespace std;

struct marchOpts {
  int smooth;
  vec3 origin; // origin of volumetric data
  double scale; // cube diagonal unit vector scale (dist. btwn data pts)
  int nX, nY, nZ; // number of voxels in each direction
};

class MarchingCubes {
  vector<vec3> verts;
  vector<int> faces;
  vec3 unitCube;
  vec3 origin;
  vector<int> vertnums;
  int nX;
  int nY;
  int nZ;
  public:
    void march(vector<vector<vector<uint8_t>>> data, marchOpts &opts);
    int getVertex(int i, int j, int k, int code, int p1, int p2);
    void laplacianSmooth(int numiter);
    vector<vec3> getVerts();
    vector<int> getFaces();
}; 




#endif
