import sys

Atoms = ["H","Li","Na","K","C","N","O","F","P","S","CL","BR","SE","ZN","CU","NI","X"]

inputFile = str(sys.argv[1])
outputFile = str(sys.argv[2])

output = open(outputFile,"w")
with open(inputFile) as input:
    for line in input:
        split = line.split()
        if ("ANISOU" in split[0] or not(("ATOM" in split[0]) or ("HETATM" in split[0]))):
            pass
        #elif len(line.split()) < 12:
        #    pass
        else:
            #output.write(line);
            #line = line.split()
            # if (line[-1] in Atoms):
            # filtered = [line[-6],line[-5],line[-4],line[-1],line[1]]
            #if (not('B' == line[16])):
            if (line[16] == ' ' or line[16] == 'A'):
            #    output.write(line)
            #if (not('B' in split[-10]) and not('B' in split[-9])):
            #    output.write(' '.join(line) + '\n')
                filtered = [line[30:38],line[38:46],line[46:54],line[76:78]]
                filtered = ' '.join(filtered)
                filtered = filtered.split()
             	output.write('\t'.join(filtered)+'\n')
output.close()
