import sys

inputFile = str(sys.argv[1])
outputFile = str(sys.argv[2])

output = open(outputFile,"w")
with open(inputFile) as input:
    for line in input:
        if "ANISOU" in line:
            print line
            pass
        elif len(line.split()) < 12:
            pass
        else:
            line = line.split()
            filtered = [line[1],line[6],line[7],line[8],line[11]]
            output.write('\t'.join(line)+'\n')
output.close()
