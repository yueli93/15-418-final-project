#ifndef VEC3_H
#define VEC3_H

struct vec3 {
  double x, y, z;
  int atomId;
};

void copy(vec3 &v, vec3 &toCopy);
void add(vec3 &v, vec3 &add);
void subtract(vec3 &v, vec3 &add);
void multiplyScalar(vec3 &v, double scalar);
void divideScalar(vec3 &v, double scalar);
double distanceToSquared(vec3 &v1, vec3 &v2); 
double distanceTo(vec3 &v1, vec3 &v2);
bool isEqual(vec3 &v1, vec3 &v2);

#endif
