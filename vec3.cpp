#include <math.h>
#include <vec3.h>

void copy(vec3 &v, vec3 &toCopy) {
    v.x = toCopy.x;
    v.y = toCopy.y;
    v.z = toCopy.z;
}

void add(vec3 &v, vec3 &add) {
  v.x += add.x;
  v.y += add.y;
  v.z += add.z;
}

void subtract(vec3 &v, vec3 &add) {
  v.x -= add.x;
  v.y -= add.y;
  v.z -= add.z;
}

void multiplyScalar(vec3 &v, double scalar) {
  v.x *= scalar;
  v.y *= scalar;
  v.z *= scalar;
}

void divideScalar(vec3 &v, double scalar) {
  if(scalar != double(0.0)) {
    v.x /= scalar;
    v.y /= scalar;
    v.z /= scalar;
  }
  else {
    v.x = double(0.0);
    v.y = double(0.0);
    v.z = double(0.0);
  }
}

double distanceToSquared(vec3 &v1, vec3 &v2) {
  double dx = v1.x - v2.x;
  double dy = v1.y - v2.y;
  double dz = v1.z - v2.z;

  return (dx*dx) + (dy*dy) + (dz*dz);
}

double distanceTo(vec3 &v1, vec3 &v2) {
  return sqrt(distanceToSquared(v1, v2)); 
}

bool isEqual(vec3 &v1, vec3 &v2) {
  return (v1.x == v2.x) && (v1.y == v2.y) && (v1.z == v2.z);
}

void copy(vec3 v, vec3 &cpy) {
  v.x = cpy.x;
  v.y = cpy.y;
  v.z = cpy.z;
}
