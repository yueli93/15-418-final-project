
EXECUTABLE := render

CU_FILES   := cudaRenderer.cu

CU_DEPS    :=

CC_FILES   := main.cpp vec3.cpp MarchingCubes.cpp

#LOGS	   := logs

###########################################################

ARCH=$(shell uname | sed -e 's/-.*//g')
OBJDIR=objs
CXX=g++ -m64
CXXFLAGS=-O3 -std=c++11 -Wall -g -pthread

INCLUDES 	 := .
LIBS       := 
FRAMEWORKS :=

ifeq ($(ARCH), Darwin)
# Building on mac
#NVCCFLAGS=-O3 -m64 -arch compute_10
#FRAMEWORKS += OpenGL GLUT
#LDFLAGS=-L/usr/local/cuda/lib/ -lcudart
else
# Building on Linux
#NVCCFLAGS=-O3 -m64 -arch compute_20
#LIBS += GL glut cudart
#LDFLAGS=-L/usr/local/cuda/lib64/ -lcudart
endif

LDINCLUDES := $(addprefix -I, $(INCLUDES))
LDLIBS  := $(addprefix -l, $(LIBS))
LDFRAMEWORKS := $(addprefix -framework , $(FRAMEWORKS))

NVCC=nvcc

OBJS=$(addprefix $(OBJDIR)/,$(notdir $(CC_FILES:.cpp=.o)))


.PHONY: dirs clean

default: $(EXECUTABLE)

dirs:
		mkdir -p $(OBJDIR)/

clean:
		rm -rf $(OBJDIR) *~ $(EXECUTABLE) # $(LOGS)

check:	default
		./checker.pl

$(EXECUTABLE): dirs $(OBJS)
		$(CXX) $(CXXFLAGS) -o $@ $(OBJS) $(LDINCLUDES) $(LDFLAGS) $(LDLIBS) $(LDFRAMEWORKS)

$(OBJDIR)/%.o: %.cpp
		$(CXX) $< $(CXXFLAGS) -c -o $@ $(LDINCLUDES)

$(OBJDIR)/%.o: %.cu
		$(NVCC) $< $(NVCCFLAGS) -c -o $@ $(LDINCLUDES)
