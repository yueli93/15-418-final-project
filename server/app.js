var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var http = require('http');
var fs = require('fs');
var spawn = require('child_process').spawn;

var app = express();

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// app.use(express.static(path.join(__dirname, 'public')));

//var router = app.Router();
app.post('/render/:id', function (req, res, next) {
  console.log("received request for id", req.params.id);
  var pdb_id = req.params.id;
  var pdb_parsed_file = "../pdb_parsed/"+pdb_id;

  // check if parsed file exists
  fs.exists(pdb_parsed_file, function (exists) {
    if(exists) {
      render(pdb_id, res);
    }
    else {
      //res.send("oh hai, we no haf pdb");
      render("", res);
      // get PDB file, parse it, then render
      // http.get({
      //   host: 'pdb.org',
      //   path: '/pdb/files/'+pdb_id+'.pdb'
      // }, function (pdbResponse) {
      //   pdbResponse.on('end', function () {
      //     // TODO: write pdb file to disk, or pipe into scrub program
      //     var parsePdbFn = spawn('../scrub.py')
      //   });
      // });
    }
  });


});

function render(pdb_id, res) {
  var renderFn = spawn('../render', ['../pdb_parsed/small']);
  // var renderFn = spawn('../render', ['../pdb_parsed/'+pdb_id]);
  renderFn.stdout.pipe(res);
  renderFn.stderr.pipe(process.stdout);
}

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
